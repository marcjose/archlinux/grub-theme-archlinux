# Grub Themes for Archlinux

Forked from https://github.com/xenlism/Grub-themes. Contains the Archlinux themes only but the background image was converted to PNG to resolve https://github.com/xenlism/Grub-themes/issues/1.


## Install 
Copy the folder for your resolution to `/boot/grub/themes/` and modify `/etc/default/grub` to contain the line:

> \# Adapt the theme name as needed  
> GRUB_THEME="/boot/grub/themes/archlinux-hd/theme.txt"   

and finally run `grub-mkconfig -o /boot/grub/grub.cfg`.
